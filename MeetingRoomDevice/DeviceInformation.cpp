#include <Arduino.h>
#include "DeviceInformation.h"

String DeviceInformation::ToJSONString(){
    return  "{"
            "\"RoomNumber\":\""+RoomNumber+"\","
            "\"Owner\":\""+Owner+"\","
            "\"ManufactureDate\":\""+ManufactureDate+"\","
            "\"ManufacturedBy\":\""+ManufacturedBy+"\","
            "\"DeviceID\":\""+DeviceID+"\","
            "\"SSID\":\""+SSID+"\","
            "\"API_URL\":\""+API_URL+"\","
            "\"MAC\":\""+MAC+"\","
            "\"DeviceMode\":"+DeviceMode+
            "}";
}