#include<Arduino.h>
#include"EEPROMController.h"
#include"EEPROMDriver.h"

char* EEPROMController::GetSSID(){
    return _memory.GetSSID();
}
char* EEPROMController::GetPassword(){
    return _memory.GetPassword();
}
String EEPROMController::GetRoomNumber(){ 
    return _memory.GetRoomNumber();
}
String EEPROMController::GetUrl(){
    return _memory.GetUrl();
}
int EEPROMController::GetMode(){
    return _memory.GetDeviceMode();
}
bool EEPROMController::SetLogin(String ssid,String pwd){
    return _memory.UpdateCred(ssid,pwd);
}
bool EEPROMController::SetRoomNumber(String no){
    return _memory.UpdateRoomNo(no);
}
bool EEPROMController::SetUrl(String url){
    return _memory.UpdateUrl(url);
}
bool EEPROMController::SetMode(String mode){
    return _memory.UpdateMode(mode);
}
void EEPROMController::reset(){
    _memory.Clear(0,512);
}
