#ifndef controller_h
#define controller_h
#include<Arduino.h>
#include <ESP8266WebServer.h>
#include "EEPROMDriver.h"

class EEPROMController{
    public :
        char* GetSSID();
        char* GetPassword();
        String GetRoomNumber();
        String GetUrl();
        int GetMode();
        bool SetLogin(String ssid,String pwd);
        bool SetRoomNumber(String no);
        bool SetUrl(String url);
        bool SetMode(String mode);
        void reset();
    private :
        EEPROMDriver _memory;
};
#endif