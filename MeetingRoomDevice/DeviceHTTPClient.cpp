#include <ESP8266HTTPClient.h>
#include <Arduino.h>
#include "DeviceHTTPClient.h"

bool DeviceHTTPClient::Post(String url ,String payload){
    HTTPClient http;
    http.begin(BaseUrl+url); 
    http.addHeader("Content-Type","application/json");
    int httpCode = http.POST(payload);
    http.end();
    if(httpCode == HTTP_CODE_NO_CONTENT) 
    {
        return true;
    }
    return false;
}
bool DeviceHTTPClient::SetBaseUrl(String url){
    BaseUrl = url;
}