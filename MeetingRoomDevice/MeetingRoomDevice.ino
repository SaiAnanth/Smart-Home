#include "ESP8266WiFi.h"
#include <ESP8266WebServer.h>
#include "EEPROMController.h"
#include "DeviceHTTPClient.h"
#include "DeviceInformation.h"
#include "Payload.h"
#include "WiFiSetup.h"
#include "DeviceMode.h"

#define OWNER "GGK TECHNOLOGIES"
#define MANUFACTUREDATE "7/3/2017"
#define MANUFACTUREDBY "GGK TECHNOLOGIES"

#define DEFAULTURL "http://ggkmeetingroom.azurewebsites.net/api/Sensor"

#define PWD "Welcome2ggk"

const String DEVICEID = "GGK_MRD_";
ESP8266WebServer server(80);
EEPROMController controllerObj;
DeviceHTTPClient clientObj;
DeviceInformation sensorDetails;
Payload devicePayload;
WiFiSetup wifi;

const int sensor_Pin = D2;   //GPIO4 
const int connectionStatus_Pin =  D6;//GPIO12
const int modeSwitch_Pin =D7;//GPIO13
const int rst_Pin = D5;//GPIO14
const int control_Pin = D1;//GPIO5

int previousState=LOW;

void setup() {

  Serial.begin(115200);
  Serial.println("Setup begin ");
  pinMode(connectionStatus_Pin, OUTPUT);
  pinMode(control_Pin,OUTPUT);
  pinMode(sensor_Pin, INPUT);
  pinMode(modeSwitch_Pin,INPUT_PULLUP);
  pinMode(rst_Pin,INPUT_PULLUP);
  
  digitalWrite(connectionStatus_Pin, LOW);
  digitalWrite(control_Pin, LOW);
  digitalWrite(D6, LOW);
  
  attachInterrupt(digitalPinToInterrupt(modeSwitch_Pin), WiFiSetup , CHANGE);
  Init();
  Serial.println("server Setup start");
  Serial.println("server Setup end");
  WiFiSetup();
  Serial.println("Setup end ");
      
}



void loop() {
  if(digitalRead(modeSwitch_Pin) == HIGH){
    server.handleClient();  
  }
  
  if(WiFi.status() == WL_CONNECTED)
  {
    digitalWrite(connectionStatus_Pin, HIGH);
  }
  else
  {
    digitalWrite(connectionStatus_Pin, LOW); 
  }
  SensorUpdate();
  reset();
}


void Init(){
  DetailsInit();
  PayloadInit();
}


void UrlInit(){
  Serial.println("url init");
  String url =controllerObj.GetUrl();
  if(url.length() == 0){
    Serial.println(DEFAULTURL);
    clientObj.SetBaseUrl(DEFAULTURL);
    sensorDetails.API_URL = DEFAULTURL;
  }
  else {
    Serial.println(url);
    clientObj.SetBaseUrl(url);
    sensorDetails.API_URL = url;
  }
}
void ModeInit(){
  int mode =controllerObj.GetMode();
  Serial.println(mode);
  if(mode < 3 && mode > 0){
    sensorDetails.DeviceMode = mode;
  }
  else {
    sensorDetails.DeviceMode = WIFI_CONTROL;
  }
}
void RoomNumberInit(){
  String roomno =controllerObj.GetRoomNumber();
  if(roomno.length() == 0){
    devicePayload.RoomNumber = "NA";
    sensorDetails.RoomNumber = "NA";
  }
  else {
    devicePayload.RoomNumber = roomno;
    sensorDetails.RoomNumber = roomno;
  }
}

void WiFiSetup(){
  if(digitalRead(modeSwitch_Pin) == HIGH) {
    wifi.Setup(WIFI_AP);
    char id[15] ;
    String str =DEVICEID+ sensorDetails.RoomNumber;
    str.toCharArray(id, 15);
    WiFi.softAP(id,PWD);
    ServerInit();
  }
  else {
    Serial.printf("Connection status: %d\n", WiFi.status());
    wifi.Setup(WIFI_STA,controllerObj.GetSSID(),controllerObj.GetPassword());
  }
}

void PayloadInit(){
  devicePayload.Status =0;
}

void DetailsInit(){
  sensorDetails.Owner = OWNER;
  sensorDetails.ManufactureDate = MANUFACTUREDATE;
  sensorDetails.ManufacturedBy = MANUFACTUREDBY;
  sensorDetails.MAC =WiFi.softAPmacAddress().c_str();
  ModeInit();
  RoomNumberInit();
  sensorDetails.DeviceID = DEVICEID+sensorDetails.RoomNumber;
  UrlInit();
  sensorDetails.SSID = controllerObj.GetSSID();
  Serial.println(sensorDetails.SSID);
}

void SensorUpdate(){
    devicePayload.Status = digitalRead(sensor_Pin);
    //Serial.printf("status %d /n",devicePayload.Status);
    if(previousState!=devicePayload.Status)
    {
      if(devicePayload.Status == HIGH)
      {
        digitalWrite(control_Pin,HIGH);
        Serial.printf("control status: %d/n", digitalRead(control_Pin));
      }
      else
      {
        digitalWrite(control_Pin,LOW);
        Serial.printf("control status: %d\n", digitalRead(control_Pin));
      }
      if(WiFi.status() == WL_CONNECTED)
      {
        if(sensorDetails.DeviceMode == WIFI || sensorDetails.DeviceMode ==WIFI_CONTROL)
        {
          httpPOST((String)devicePayload.ToJSONString());          
        }
      }
    }
    previousState=devicePayload.Status;
    delay(100);
}
void SetLogin() {
  if(controllerObj.SetLogin(server.arg("SSID"),server.arg("PASSWORD")))
  {
    sensorDetails.SSID = server.arg("SSID");
    Serial.println("login triggered "+ server.arg("SSID") );
    server.send(200, "text/html","login sucessful");
  }
  else
  {
    server.send(404, "text/html","login unsucessful");  
  }
}
void SetUrl(){
  Serial.println("seturl");
  if(controllerObj.SetUrl(server.arg("URL"))){
    UrlInit();
    server.send(200, "text/html","URL set sucessful");
  }
  else{
    server.send(404, "text/html","URL set unsucessful");
  }
}
void SetRoomNumber(){
  if(controllerObj.SetRoomNumber(server.arg("RoomNo"))){
    RoomNumberInit();
    server.send(200, "text/html","Room Number set sucessful");
    WiFiSetup();
  }
  else{
    server.send(404, "text/html","Room Number set unsucessful");
  }
}
void SetMode(){
  if(controllerObj.SetMode(server.arg("DeviceMode")))
  {
    server.send(200, "text/html","Mode set sucessful");
    ModeInit();

  }
  else
  {
    server.send(404, "text/html","Mode set unsucessful");
  }
}
void GetDetails(){
  server.send(200,"text/html",sensorDetails.ToJSONString());
}


void reset(){
  if(digitalRead(rst_Pin) == LOW)
  {
    delay(5000);
    if(digitalRead(rst_Pin)==LOW)
    {
      controllerObj.reset();
      DetailsInit();

      for(int i=0;i<10;i++){
        digitalWrite(connectionStatus_Pin,HIGH);
        delay(100);
        digitalWrite(connectionStatus_Pin,LOW);
        delay(100);
      }
    }
  }
}


void httpPOST(String stat )
{
  Serial.println("url "+controllerObj.GetUrl());
  Serial.println(stat);
 if(clientObj.Post("",stat)) 
  {
      Serial.println("Post sucessful");   
  }  
  else 
  {
      Serial.println("post unnsucessful"); 
  }
} 
void ServerInit(){
  server.on("/login",HTTP_POST,SetLogin);
  server.on("/url",HTTP_POST,SetUrl);
  server.on("/details",HTTP_GET,GetDetails);
  server.on("/roomNo",HTTP_POST,SetRoomNumber);
  server.on("/deviceMode",HTTP_POST,SetMode);
  server.begin();
}


