#ifndef Payload_h
#define Payload_h
#include<Arduino.h>

class Payload{
    public :
        String RoomNumber;
        int Status;
        String ToJSONString();
};
#endif