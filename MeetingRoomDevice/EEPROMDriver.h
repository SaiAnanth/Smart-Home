#ifndef EEPROMDriver_h
#define EEPROMDriver_h
#include<Arduino.h>
#include <EEPROM.h>

#define SSID_SIZE 20
#define PASSWORD_SIZE 20
#define URL_SIZE 100
#define SSID_ADDR 1
#define ROOM_SIZE 10
#define PASSWORD_ADDR  21 
#define URL_ADDR  41 
#define ROOM_ADDR 141
#define MODE_ADDR 151
#define MODE_SIZE 1 
class EEPROMDriver{
    public:
        void Clear(int start ,int stop);
        bool UpdateCred(String ssid,String password);
        bool UpdateUrl(String url);
        bool UpdateRoomNo(String roomNo);
        bool UpdateMode(String mode);
        char* GetSSID();
        char* GetPassword();
        char* GetUrl();
        String GetRoomNumber();
        int GetDeviceMode();
    private :
        char SSID[SSID_SIZE];
        char Password[PASSWORD_SIZE];
        char URL[URL_SIZE];
        char RoomNo[ROOM_SIZE];
        int Mode;
};
#endif
