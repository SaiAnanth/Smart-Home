#ifndef DeviceInformation_h
#define DeviceInformation_h
#include<Arduino.h>

class DeviceInformation{
    public:
        String RoomNumber;
        String Owner;
        String ManufacturedBy;
        String ManufactureDate;
        String DeviceID;
        String SSID;
        String API_URL;
        String MAC;
        int DeviceMode;
        String ToJSONString();
};
#endif