
#include <Arduino.h>
#include "ESP8266WiFi.h"
#include "WiFiSetup.h"


void WiFiSetup::Setup(int mode,char* ssid,char* password){
    if(mode == WIFI_AP)
    {
        IPAddress local_IP(192,168,4,24);
        IPAddress gateway(192,168,4,9);
        IPAddress subnet(255,255,255,0);
        WiFi.mode(WIFI_AP);
        //WiFi.softAP(SSID,PASSWORD);
        WiFi.softAPConfig(local_IP, gateway, subnet);
    }
    else if(mode =WIFI_STA)
    {
        WiFi.mode(WIFI_STA);
        WiFi.disconnect();
        WiFi.begin(ssid,password);
    }
}