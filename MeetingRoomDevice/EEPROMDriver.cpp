#include"EEPROMDriver.h"
#include<Arduino.h>

void EEPROMDriver::Clear(int start ,int stop){
    EEPROM.begin(512);
    for(int i=start;i<stop;i++){
        EEPROM.write(i,0);
    }
    EEPROM.end();
}
bool EEPROMDriver::UpdateCred(String ssid,String password){
    if(ssid.length()>SSID_SIZE||password.length()>PASSWORD_SIZE) return false;
    Clear(SSID_ADDR,SSID_ADDR+SSID_SIZE);
    Clear(PASSWORD_ADDR ,PASSWORD_SIZE+PASSWORD_SIZE);
    
    EEPROM.begin(512);
    
    int addr=SSID_ADDR;
    for(int i=0;ssid[i]!='\0';i++,addr++)   EEPROM.write(addr,ssid[i]);
    
    addr=PASSWORD_ADDR;
    for(int i=0;password[i]!='\0';i++,addr++)   EEPROM.write(addr,password[i]);
    
    EEPROM.commit();    
    EEPROM.end();
    
    return true;
}
bool EEPROMDriver::UpdateUrl(String url){
    if(url.length()>URL_SIZE) return false;
    Clear(URL_ADDR,URL_ADDR+URL_SIZE);
    EEPROM.begin(512);
    int addr =URL_ADDR;
    for(int i=0;url[i]!='\0';i++,addr++)    EEPROM.write(addr,url[i]);
    EEPROM.commit();
    EEPROM.end();
    return true;
}
bool EEPROMDriver::UpdateRoomNo(String roomNo){
    if(roomNo.length()>ROOM_SIZE) return false;
    Clear(ROOM_ADDR,ROOM_ADDR+ROOM_SIZE);
    EEPROM.begin(512);
    int addr =ROOM_ADDR;
    for(int i=0;roomNo[i]!='\0';i++,addr++)    EEPROM.write(addr,roomNo[i]);
    EEPROM.commit();
    EEPROM.end();
    return true;
    
}
bool EEPROMDriver::UpdateMode(String mode){
    if(mode.length()>1) return false;
    Clear(MODE_ADDR,MODE_ADDR+MODE_SIZE);
    EEPROM.begin(512);
    EEPROM.write(MODE_ADDR,mode[0]-48);
    EEPROM.commit();
    EEPROM.end();
    return true;
}

char* EEPROMDriver::GetSSID(){
    EEPROM.begin(512);
    int addr=SSID_ADDR;
    int i=0;
    char c;
    do{
        c = (char)EEPROM.read(addr);
        SSID[i] =c;
        i++;
        addr++;
    }while(c !='\0');
    EEPROM.end();
    return SSID;
}
char* EEPROMDriver::GetPassword(){
    EEPROM.begin(512);
    int addr=PASSWORD_ADDR;
    int i=0;
    char c;
    do{
        c =(char)EEPROM.read(addr);
        Password[i]=c;
        i++;
        addr++;
    }while(c!='\0');
    EEPROM.end();
    return Password;
}
char* EEPROMDriver::GetUrl(){
    EEPROM.begin(512);
    int addr=URL_ADDR;
    int i=0;
    char c;
    do{
        c =(char)EEPROM.read(addr);
        URL[i]=c;
        i++;
        addr++;
    }while(c!='\0');
    EEPROM.end();
    return URL;
}
String EEPROMDriver::GetRoomNumber(){
    EEPROM.begin(512);
    int addr=ROOM_ADDR;
    int i=0;
    char c;
    do{
        c =(char)EEPROM.read(addr);
        RoomNo[i]=c;
        i++;
        addr++;
    }while(c!='\0');
    EEPROM.end();
    return RoomNo;
}
int EEPROMDriver::GetDeviceMode(){
    EEPROM.begin(512);
    return EEPROM.read(MODE_ADDR);
}

